class Page:
    """
    Класс Страница
    Поля: номер страницы, контент
    Методы:
        вывести контент,
        поиск строки в контенте (содержится ли строка в контенте?)
    """

    def __init__(self, page_num, content):
        self.page_number = page_num
        self.content = content

    def print_content(self):
        print(self.content)

    def find_str_in_content(self, str_to_find):
        return str_to_find in self.content

    def __str__(self):
        return self.content


class Book:
    """
    Класс Книга
    Поля: страницы, название, автор, год издания
    Методы:
        вывести весь контент,
        добавить страницу в конец,
        получить количество страниц,
        вернуть все четные страницы
        __invert__: вернуть результат реверса строки с названием
                    (например, было "Крутая Книга", а стало "Агинк Яатурк").
        __add__: добавить к году число
    """

    def __init__(self, title, author, year):
        self.pages = []
        self.title = title
        self.author = author
        self.year = year

    def print_content(self):
        for page in self.pages:
            page.print_content()

    def append_page(self, page):
        self.pages.append(page)

    def page_count(self):
        return len(self.pages)

    def get_even_pages(self):
        return [p for p in self.pages if p.page_number % 2 == 0]


def main():
    my_book = Book('Моя книга', 'Простой человек', 2020)
    print(f'title: {my_book.title}')
    print(f'author: {my_book.author}')
    print(f'year: {my_book.year}')
    print(f'page_count: {my_book.page_count()}')
    print('print_content:')
    my_book.print_content()

    print('*' * 50)

    p1 = Page(1, 'Жила-была собачка.')
    p2 = Page(2, 'Собачку звали Петя.')
    p3 = Page(3, 'Петя был счастлив.')

    p1.print_content()
    p2.print_content()
    p3.print_content()

    print('*' * 50)

    print(f'"Собачка" в p1: {p1.find_str_in_content("Собачка")}')
    print(f'"собачка" в p1: {p1.find_str_in_content("собачка")}')

    print('*' * 50)

    my_book.append_page(p1)
    my_book.append_page(p2)
    my_book.append_page(p3)
    print(f'page_count: {my_book.page_count()}')
    print('print_content:')
    my_book.print_content()

    print('*' * 50)

    for p in my_book.get_even_pages():
        print(p)


if __name__ == '__main__':
    main()
