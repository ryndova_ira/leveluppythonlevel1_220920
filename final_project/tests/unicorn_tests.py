from datetime import date

import pytest

from final_project.animal import Animal
from final_project.unicorn import Unicorn


class TestUnicorn:

    def setup_method(self):
        Unicorn.count = 0

    def test_init_ok(self):
        unicorn = Unicorn('Лиза', 2, 'черный', 'розовый')
        assert unicorn.name == 'Лиза'
        assert unicorn.age == 2
        assert unicorn.color == 'черный'
        assert unicorn.firework_color == 'розовый'

        assert isinstance(unicorn, Animal)

        assert Unicorn.count == 1

    def test_from_birth_year_ok(self):
        unicorn = Unicorn.from_birth_year('Лиза', 2018, 'черный', 'розовый')
        assert unicorn.name == 'Лиза'
        assert unicorn.age == date.today().year - 2018
        assert unicorn.color == 'черный'
        assert unicorn.firework_color == 'розовый'

        assert isinstance(unicorn, Animal)

        assert Unicorn.count == 1

    def test_toucan_count_ok(self):
        assert Unicorn.count == 0

        num = 20
        for i in range(num):
            Unicorn.from_birth_year('Лиза', 2018, 'черный', 'розовый')

        assert Unicorn.count == num

    @pytest.mark.parametrize("name, age, color, firework_color",
                             [
                                 ('Lizzy', 2018, 'black', 'pick'),
                                 ('Vasiliy', 2010, 'white', 'black'),
                                 ('Pete', 2019, 'blue', 'gold'),
                             ])
    def test_str_ok(self, name, age, color, firework_color):
        unicorn = Unicorn(name, age, color, firework_color)
        assert str(
            unicorn) == f'Unicorn: Имя: {name} | Возраст: {age} | Цвет: {color} | Цвет звездочек: {firework_color}'

    def test_start_firework(self):
        unicorn = Unicorn.from_birth_year('Лиза', 2018, 'черный', 'розовый')
        assert unicorn.start_firework() == 'Единорог Лиза запустил звездочки розовый цвета!!!'
