def zip_names(name_list, surname_set):
    """
    Функция zip_names.

    Принимает 2 аргумента: список с именами и множество с фамилиями.

    Возвращает список с парами значений из каждого аргумента.

    Если вместо list передано что-то другое, то возвращать строку 'First arg must be list!'.
    Если вместо set передано что-то другое, то возвращать строку 'Second arg must be set!'.

    Если list пуст, то возвращать строку 'Empty list!'.
    Если set пуст, то возвращать строку 'Empty set!'.

    Если list и set различного размера, обрезаем до минимального (стандартный zip).
    """

    if type(name_list) != list:
        return 'First arg must be list!'

    if type(surname_set) != set:
        return 'Second arg must be set!'

    if len(name_list) == 0:
        return 'Empty list!'

    if len(surname_set) == 0:
        return 'Empty set!'

    return list(zip(name_list, surname_set))


def main():
    yn = 'yes'
    while True:
        if yn == 'да' or yn == 'yes':
            my_list_name_str = input('Введите имена через пробел: ')
            if len(my_list_name_str) != 0:
                my_list_name = my_list_name_str.split(' ')
            else:
                my_list_name = ()
            my_set_surname_str = input('Введите фамилии через пробел: ')
            if len(my_set_surname_str) != 0:
                my_set_surname_list = my_set_surname_str.split(' ')
            else:
                my_set_surname_list = ()
            my_set_surname = set(my_set_surname_list)
            print(zip_names(my_list_name, my_set_surname))
            yn = input('Продолжить? Введите yes(да) или no(нет): ')
        elif yn == 'нет' or yn == 'no':
            break
        else:
            print('Некорректная команда!')
            yn = input('Продолжить? Введите yes(да) или no(нет): ')


if __name__ == '__main__':
    main()

print('Конец!')
