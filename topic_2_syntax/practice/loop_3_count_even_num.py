def count_even_num(number):
    """
    Функция count_even_num.

    Принимает натуральное число (целое число > 0).
    Верните количество четных цифр в этом числе.
    Если число равно 0, то вернуть "Must be > 0!".
    Если число не целое (не int, а другой тип данных), то вернуть "Must be int!".
    """

    if type(number) != int:
        return "Must be int!"
    elif number <= 0:
        return "Must be > 0!"
    else:
        count = 0
        for char in str(number):
            if int(char) % 2 == 0:
                # Четное, если делится на 2 без остатка
                count = count + 1
        return count


if __name__ == '__main__':
    while True:
        num_str = input('Введите натуральное число (для завершения 0): ')
        if num_str.isdigit():
            num = int(num_str)
            if num == 0:
                break
            else:
                print(count_even_num(num))
        else:
            print('Некорректно введено число!')

    print('Спасибо за участие!')
