def pow_odd(n):
    """
    Функция pow_odd.

    Принимает число n.

    Возвращает словарь длиной n, в котором
        ключ - это нечетное число в диапазоне от 0 до n (включая)
        значение - квадрат ключа.

    Пример: n = 5, четные числа [1, 3, 5], результат {1: 1, 3: 9, 5: 25}.

    Если n не является int, то вернуть строку 'Must be int!'.
    """
    if type(n) != int:
        return 'Must be int!'

    return {k: k ** 2 for k in range(n + 1) if k % 2 == 1}


def main():
    yn = 'yes'
    while True:
        if yn == 'да' or yn == 'yes':
            long = int(input('Введите длину словаря: '))
            print(pow_odd(long))
            yn = input('Продолжить? Введите yes(да) или no(нет): ')
        elif yn == 'нет' or yn == 'no':
            break
        else:
            print('Некорректная команда!')
            yn = input('Продолжить? Введите yes(да) или no(нет): ')


if __name__ == '__main__':
    main()

print('Конец!')
