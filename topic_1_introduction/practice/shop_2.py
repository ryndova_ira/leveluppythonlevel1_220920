"""
Ввод данных с клавиатуры:
    - Магазин (наименование)
    - Товар (наименование)
    - Количество (товара)

Вывод данных в формате:
"Магазин назвается '<Магазин>'. В нем имеется товар <Товар> в количестве <Количество>."
"""

# Ввод данных
shop_name = input('Наименование магазина: ')
item_name = input('Наименование товара: ')
item_count = input('Количество товара: ')

# Вывод данных
print("Магазин назвается '", shop_name,
      "'. В нем имеется товар ", item_name,
      " в количестве ", item_count, ".",
      sep='')

