def print_diff_count(str1, str2):
    """
    Функция print_diff_count.

    Принимает две строки.

    Вывести большую по длине строку столько раз,
    насколько символов отличаются строки.

    Если строки одинаковой длины, то вывести строку "Equal!".
    """
    str_diff = len(str1) - len(str2)
    if str_diff > 0:
        print(str1 * str_diff)
    elif str_diff < 0:
        print(str2 * (-str_diff))
    else:
        print('Equal!')


def main():
    is_continue = ''
    while is_continue != 'yes':
        string1 = input("Введите строку 1: ")
        string2 = input("Введите строку 2: ")
        print_diff_count(string1, string2)
        is_continue = input("Хотите закончить (yes)? ")

    print("Спасибо за участие!")


if __name__ == '__main__':
    main()
