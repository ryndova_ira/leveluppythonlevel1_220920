from final_project.animal import Animal


class TestAnimal:
    my_animal_feats = None

    @classmethod
    def setup_class(cls):
        cls.my_animal_feats = ('Вася', 5, 'розовый')

    def setup_method(self):
        Animal.count = 0

    def test_init_ok(self):
        my_animal = Animal(*self.__class__.my_animal_feats)
        assert my_animal.name == 'Вася'
        assert my_animal.age == 5
        assert my_animal.color == 'розовый'
        assert my_animal.count == 1

    def test_str_ok(self):
        my_animal = Animal(*self.__class__.my_animal_feats)
        assert str(my_animal) == 'Имя: Вася | Возраст: 5 | Цвет: розовый'

    def test_count_ok(self):
        num = 20
        for i in range(num):
            Animal(*self.__class__.my_animal_feats)

        assert Animal.count == num
