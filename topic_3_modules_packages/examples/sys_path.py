import sys

print(sys.path)
# Ubuntu:
# ['/media/ira/...leveluppythonlevel1/Examples_L5', '/media/ira/...leveluppythonlevel1',
# '/snap/pycharm-professional/198/plugins/python/helpers/pycharm_display', '/usr/lib/python37.zip',
# '/usr/lib/python3.7', '/usr/lib/python3.7/lib-dynload', '/home/ira/.local/lib/python3.7/site-packages',
# '/usr/local/lib/python3.7/dist-packages', '/usr/lib/python3/dist-packages',
# '/snap/pycharm-professional/198/plugins/python/helpers/pycharm_matplotlib_backend']

# Windows:
# ['', '/usr/lib/python35.zip', '/usr/lib/python3.5', '/usr/lib/python3.5/plat-x86_64-linux-gnu',
# '/usr/lib/python3.5/lib-dynload', '/usr/local/lib/python3.5/dist-packages', '/usr/lib/python3/dist-packages']