def print_3th_symbols(string):
    """
    Функция print_3th_symbols.

    Принимает строку.
    Вывести третий, шестой, девятый и так далее символы.
    """
    print(string[2::3])

    # Если нужно вывести 2-й, 6-й и 9-й символы,
    # но мы не знаем какой длины строка
    # print(string[1:2], string[5:6], string[8:9])


def main():
    is_continue = ''
    while is_continue != 'yes':
        string = input("Введите строку: ")
        print_3th_symbols(string)
        is_continue = input("Хотите закончить (yes)? ")

    print("Спасибо за участие!")


if __name__ == '__main__':
    main()
