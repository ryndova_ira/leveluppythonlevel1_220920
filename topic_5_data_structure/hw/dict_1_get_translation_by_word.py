def get_translation_by_word(ru_eng, ru_word):
    """
    Функция get_translation_by_word.

    Принимает 2 аргумента:
        ru-eng словарь содержащий {ru_word: [eng_1, eng_2 ...], ...}
        слово для поиска в словаре (ru).

    Возвращает все варианты переводов (list), если такое слово есть в словаре,
    если нет, то "Can't find Russian word: {word}".

    Если вместо словаря передано что-то другое, то возвращать строку "Dictionary must be dict!".
    Если вместо строки для поиска передано что-то другое, то возвращать строку "Word must be str!".

    Если словарь пустой, то возвращать строку "Dictionary is empty!".
    Если строка для поиска пустая, то возвращать строку "Word is empty!".
    """
    if type(ru_eng) != dict:
        return "Dictionary must be dict!"

    if type(ru_word) != str:
        return "Word must be str!"

    if len(ru_eng) == 0:
        return "Dictionary is empty!"

    if len(ru_word) == 0:
        return "Word is empty!"

    return ru_eng.get(ru_word, f"Can't find Russian word: {ru_word}")


def main():
    ru_eng = {"rus_1": ["eng_1", "eng_2"], "rus_2": "eng_2", "rus_3": "eng_3"}
    print(get_translation_by_word(ru_eng, "rus_1"))
    print(get_translation_by_word(ru_eng, "rus_4"))


if __name__ == "__main__":
    main()

print("Конец!")
