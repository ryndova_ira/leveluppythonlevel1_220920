def magic_mul(my_list):
    """
    Функция magic_mul.

    Принимает 1 аргумент: список my_list.

    Возвращает список, который состоит из
        [первого элемента my_list]
        + [три раза повторенных списков my_list]
        + [последнего элемента my_list].

    Пример:
        входной список [1,  ‘aa’, 99]
        результат [1, 1,  ‘aa’, 99, 1,  ‘aa’, 99, 1,  ‘aa’, 99, 99].

    Если вместо списка передано что-то другое, то возвращать строку 'Must be list!'.
    Если список пуст, то возвращать строку 'Empty list!'.
    """
    if type(my_list) != list:
        return 'Must be list!'

    if len(my_list) == 0:
        return 'Empty list!'

    new_list = [my_list[0]]
    new_list.extend(my_list * 3)
    new_list.append(my_list[-1])
    return new_list


def main():
    yn = 'yes'
    while True:
        if yn == 'да' or yn == 'yes':
            my_list_str = input('Введите список элементов через пробел: ')
            if len(my_list_str) != 0:
                my_list = my_list_str.split(' ')
                print(magic_mul(my_list))
            else:
                my_list = []
                print(my_list)
            yn = input('Продолжить? (да/нет): ')
        elif yn == 'нет' or yn == 'no':
            break
        else:
            print('Некорректная команда!')
            yn = input('Продолжить? (да/нет): ')


if __name__ == '__main__':
    main()

print('Конец!')
