def pow_even(n):
    """
    Функция pow_even.

    Принимает 1 аргумент: число n.

    Возвращает словарь длиной n, в котором
    ключ - это четное число в диапазоне от 0 до n (не включая),
    а значение - квадрат ключа.

    Пример: n = 8, четные числа [0, 2, 4, 6], результат {0: 0, 2: 4, 4: 16, 6: 36}.
    """
    if type(n) != int:
        return 'Must be int!'

    return {x: x ** 2 for x in range(n) if x % 2 == 0}


def main():
    print(pow_even(5))
    print(pow_even(8))
    my_result = pow_even(16)
    print(f'my_result: {my_result}\nkeys: {my_result.keys()}\nvalues: {my_result.values()}')


if __name__ == '__main__':
    main()
