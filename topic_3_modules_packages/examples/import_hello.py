# Импортируем функцию hello из модуля hello
import hello  # import topic_3_modules_packages.examples.hello

# importing hello module

print(hello)
# <module 'hello' from '/media/ira/.../Examples_L5/hello.py'>
type(hello)

print(hello.__file__)
# /media/ira/.../Examples_L5/hello.py

print(hello.__name__)
# hello
