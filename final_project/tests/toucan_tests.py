from datetime import date

import pytest

from final_project.animal import Animal
from final_project.toucan import Toucan


class TestToucan:
    my_animal_feats = None

    @classmethod
    def setup_class(cls):
        cls.my_animal_feats = ('Лиза', 2, 'черный', 0.75, 100)
        
    def setup_method(self):
        Toucan.count = 0

    def test_init_ok(self):
        toucan = Toucan(*self.__class__.my_animal_feats)
        assert toucan.name == 'Лиза'
        assert toucan.age == 2
        assert toucan.color == 'черный'
        assert toucan.wing_len == 0.75
        assert toucan.fly_height_m == 100

        assert isinstance(toucan, Animal)

        assert Toucan.count == 1

    def test_from_birth_year_ok(self):
        toucan = Toucan.from_birth_year('Лиза', 2018, 'черный', 0.75, 100)
        assert toucan.name == 'Лиза'
        assert toucan.age == date.today().year - 2018
        assert toucan.color == 'черный'
        assert toucan.wing_len == 0.75
        assert toucan.fly_height_m == 100

        assert isinstance(toucan, Animal)

        assert Toucan.count == 1

    def test_toucan_count(self):
        assert Toucan.count == 0

        num = 20
        for i in range(num):
            Toucan.from_birth_year('Лиза', 2018, 'черный', 0.75, 100)

        assert Toucan.count == num

    @pytest.mark.parametrize("fly_height_m, expected_result",
                             [
                                 (0, False),
                                 (0.5, False),
                                 (1, False),
                                 (300, True),
                                 (400, True)
                             ])
    def test_can_fly_ok(self, fly_height_m, expected_result):
        assert Toucan.can_fly(fly_height_m) is expected_result
