def print_fname_sname_flower_height(fname, lname, **flower_len):
    """
    Функция print_fname_sname_flower_height.

    Принимает 3 аргумента:
        имя,
        фамилию владельца цветов,
        именованные аргументы с высотой имеющихся цветов (**kwargs).

    Выводит на экран имя и фамилию, а затем цветок и высоты этого цветка (может быть несколько).

    Пример вызова функции:
        print_fname_sname_flower_height('Лидия', 'Петрова', rose=[50, 77, 80], tulp=60, magic_flower=[444, 555, 777]).
    """

    print(f'{fname} {lname}:')
    for f, l in flower_len.items():
        print(f'\t{f}: {l}')


def main():
    print_fname_sname_flower_height('Лидия', 'Петрова', rose=[50, 77, 80], tulp=60, magic_flower=[444, 555, 777])


if __name__ == '__main__':
    main()
