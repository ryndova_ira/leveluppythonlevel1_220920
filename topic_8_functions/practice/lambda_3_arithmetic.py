def main():
    """
    mapping арифметических операций с помощью lambda-функции
    В dict ключами являюся операции, а значениями lambda-функции принимающие два аргумента
    """
    arithmetic_ops = {
        '+': lambda x, y: x + y,
        '-': lambda x, y: x - y,
        '*': lambda x, y: x * y,
        '/': lambda x, y: x / y,
    }

    print(arithmetic_ops['+'](1, 2))
    print(arithmetic_ops['-'](1, 2))
    print(arithmetic_ops['*'](1, 2))
    print(arithmetic_ops['/'](1, 2))


if __name__ == '__main__':
    main()
