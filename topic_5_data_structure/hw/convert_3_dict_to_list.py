def dict_to_list(my_dict):
    """
    Функция dict_to_list.

    Принимает 1 аргумент: словарь.

    Возвращает [список ключей,
                список значений,
                количество уникальных элементов в списке ключей,
                количество уникальных элементов в списке значений].

    Если вместо словаря передано что-то другое, то возвращать строку 'Must be dict!'.
    """
    if type(my_dict) != dict:
        return 'Must be dict!'

    return list(my_dict.keys()), list(my_dict.values()), len(my_dict), len(set(my_dict.values()))


def main():
    my_dict = {'key_1': 'znach_1', 'key_2': 'znach_2', 'key_3': 'znach_3', 'key_4': 'znach_1'}
    print(my_dict)
    print(dict_to_list(my_dict))


if __name__ == '__main__':
    main()

print('Конец!')
