from topic_7_oop.hw.class_4_1_pupil import Pupil
from topic_7_oop.hw.class_4_2_worker import Worker


class School:
    """
    Класс School.

    Поля:
        список людей в школе (общий list для Pupil и Worker): people,
        номер школы: number.

    Методы:
        get_avg_mark: вернуть средний балл всех учеников школы
        get_avg_salary: вернуть среднюю зп работников школы
        get_worker_count: вернуть сколько всего работников в школе
        get_pupil_count: вернуть сколько всего учеников в школе
        get_pupil_names: вернуть все имена учеников (с повторами, если есть)
        get_unique_worker_positions: вернуть список всех должностей работников (без повторов, подсказка: set)
        get_max_pupil_age: вернуть возраст самого старшего ученика
        get_min_worker_salary: вернуть самую маленькую зп работника
        get_min_salary_worker_names: вернуть список имен работников с самой маленькой зп
                                        (список из одного или нескольких элементов)
    """

    def __init__(self, people, number):
        self.people = people
        self.number = number

    def get_avg_mark(self):
        """
        :return: вернуть средний балл всех учеников школы
        """
        all = []
        for person in self.people:
            if type(person) == Pupil:
                all.extend(person.get_all_marks())

        return sum(all) / len(all)

    def get_avg_salary(self):
        """
        :return: вернуть среднюю зп работников школы
        """
        all = []
        for person in self.people:
            if type(person) == Worker:
                all.append(person.salary)

        return sum(all) / len(all)

    def get_worker_count(self):
        """
        :return: вернуть сколько всего работников в школе
        """
        count = 0
        for person in self.people:
            if type(person) == Worker:
                count += 1

        return count

    def get_pupil_count(self):
        """
        :return: вернуть сколько всего учеников в школе
        """
        count = 0
        for person in self.people:
            if type(person) == Pupil:
                count += 1

        return count

    def get_pupil_names(self):
        """
        :return: вернуть все имена учеников (с повторами, если есть)
        """
        names = []
        for person in self.people:
            if type(person) == Pupil:
                names.append(person.name)

        return names

    def get_unique_worker_positions(self):
        """
        :return: вернуть все должности работников (без повторов)
        """
        positions = set()
        for person in self.people:
            if type(person) == Worker:
                positions.add(person.position)

        return positions

    def get_max_pupil_age(self):
        """
        :return: вернуть возраст самого старшего ученика
        """
        ages = []
        for person in self.people:
            if type(person) == Pupil:
                ages.append(person.age)

        return max(ages)

    def get_min_worker_salary(self):
        """
        :return: вернуть самую маленькую зп работника
        """
        salary = []
        for person in self.people:
            if type(person) == Worker:
                salary.append(person.salary)

        return min(salary)

    def get_min_salary_worker_names(self):
        """
        :return: вернуть список имен работников с самой маленькой зп
        """
        min_salary_names = []
        min_salary = self.get_min_worker_salary()
        for person in self.people:
            if type(person) == Worker and person.salary == min_salary:
                min_salary_names.append(person.name)

        return min_salary_names
