def list_pow_even(n):
    """
    Функция list_pow_even.

    Принимает 1 аргумент: число n.

    Возвращает список состоящий из квадратов четных чисел в диапазоне от 0 до n (не включая).

    Пример: n = 8, четные числа [0, 2, 4, 6], результат [0, 4, 16, 36].

    Если n не является int, то вернуть строку 'Must be int!'.
    """

    if type(n) != int:
        return 'Must be int!'
    return [m ** 2 for m in range(n) if m % 2 == 0]


def main():
    print(list_pow_even(5))
    print(list_pow_even(10))
    print(list_pow_even(22))


if __name__ == '__main__':
    main()
