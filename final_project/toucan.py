from datetime import date

from final_project.animal import Animal


class Toucan(Animal):
    count = 0

    def __init__(self, name, age, color, wing_len, fly_height_m):
        super().__init__(name, age, color)
        self.wing_len = wing_len
        self.fly_height_m = fly_height_m

        Toucan.count += 1

    @classmethod
    def from_birth_year(cls, name, birth_year, color, wing_len, fly_height):
        return cls(name, date.today().year - birth_year, color, wing_len, fly_height)

    def say_hi(self):
        return f'Привет! Меня зовут {self.name}!'

    def fly(self):
        return f'Я взлетел на высоту {self.fly_height_m} метров'

    def __str__(self):
        return f'Toucan: {super().__str__()} | Размах крыла: {self.wing_len} | Высота полета: {self.fly_height_m}'

    @staticmethod
    def can_fly(fly_height_m):
        return fly_height_m > 1  # может летать, если больше 1 метра

    def __eq__(self, other):
        return self.name == other['name'] \
               and self.age == other['age'] \
               and self.color == other['color'] \
               and self.wing_len == other['wing_len'] \
               and self.fly_height_m == other['fly_height_m']
