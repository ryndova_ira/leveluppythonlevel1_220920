def check_equal(n1, n2, n3):
    """
    Функция check_equal.

    Принимает 3 числа.
    Вернуть True, если среди них есть одинаковые, иначе False.
    """
    if n1 == n2 or n1 == n3 or n2 == n3:
        return True
    else:
        return False


def main():
    is_continue = ''
    while is_continue != 'yes':
        print('Введите три числа')
        num1 = int(input("Введите 1-е число: "))
        num2 = int(input("Введите 2-е число: "))
        num3 = int(input("Введите 3-е число: "))
        print(check_equal(num1, num2, num3))
        is_continue = input("Хотите закончить (yes)? ")

    print("Спасибо за участие!")


if __name__ == '__main__':
    main()
