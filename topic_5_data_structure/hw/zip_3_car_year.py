from itertools import zip_longest


def zip_car_year(car_list, year_list):
    """
    Функция zip_car_year.

    Принимает 2 аргумента: список с машинами и список с годами производства.

    Возвращает список с парами значений из каждого аргумента, если один список больше другого,
    то заполнить недостающие элементы строкой "???".

    Подсказка: zip_longest.

    Если вместо списков передано что-то другое, то возвращать строку 'Must be list!'.
    Если список (хотя бы один) пуст, то возвращать строку 'Empty list!'.
    """
    if type(car_list) != list or type(year_list) != list:
        return 'Must be list!'

    if len(car_list) == 0 or len(year_list) == 0:
        return 'Empty list!'
    return list(zip_longest(car_list, year_list, fillvalue='???'))


def main():
    yn = 'yes'
    while True:
        if yn == 'да' or yn == 'yes':
            my_list_car_str = input('Введите модели машин через пробел: ')
            if len(my_list_car_str) != 0:
                my_list_car = my_list_car_str.split(' ')
            else:
                my_list_car = ()
            my_list_year_str = input('Введите года через пробел: ')
            if len(my_list_year_str) != 0:
                my_list_year = my_list_year_str.split(' ')
            else:
                my_list_year = ()
            print(zip_car_year(my_list_car, my_list_year))
            yn = input('Продолжить? Введите yes(да) или no(нет): ')
        elif yn == 'нет' or yn == 'no':
            break
        else:
            print('Некорректная команда!')
            yn = input('Продолжить? Введите yes(да) или no(нет): ')


if __name__ == '__main__':
    main()

print('Конец!')
