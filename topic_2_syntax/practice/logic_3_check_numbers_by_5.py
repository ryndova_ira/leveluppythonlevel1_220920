def check_numbers_by_5(n1, n2, n3):
    """
    Функция check_numbers_by_5.

    Принимает 3 числа.
    Если ровно два из них меньше 5, то вернуть True, иначе вернуть False.
    """
    return (n1 < 5 and n2 < 5 and n3 >= 5) \
           or (n1 < 5 and n3 < 5 and n2 >= 5) \
           or (n3 < 5 and n2 < 5 and n1 >= 5)


def main():
    is_continue = ''
    while is_continue != 'yes':
        print('Введите три числа')
        num1 = int(input("Введите 1-е число: "))
        num2 = int(input("Введите 2-е число: "))
        num3 = int(input("Введите 3-е число: "))
        print(check_numbers_by_5(num1, num2, num3))
        is_continue = input("Хотите закончить (yes)? ")

    print("Спасибо за участие!")


if __name__ == '__main__':
    main()
