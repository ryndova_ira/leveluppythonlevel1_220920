def if_3_do(num):
    """
    Функция if_3_do.

    Принимает число.
    Если оно больше 3, то увеличить число на 10, иначе уменьшить на 10.
    Вернуть результат.
    """
    if num > 3:
        num = num + 10
    else:
        num = num - 10
    return num


if __name__ == '__main__':
    yn = 'да'
    while yn == 'да':
        n = int(input('Введите число: '))
        print(if_3_do(n))
        yn = input('Продолжить? (да/нет): ')
    print('Конец!')
