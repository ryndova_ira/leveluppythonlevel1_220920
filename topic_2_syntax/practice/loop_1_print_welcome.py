def print_welcome(n):
    """
    Функция print_welcome.

    Принимает число n.
    Выведите на экран n раз фразу "You are welcome!".
    Пример: n=3, тогда в результате "You are welcome!You are welcome!You are welcome!"
    """
    print(n * "You are welcome!")


if __name__ == '__main__':
    yn = 'да'
    while yn == 'да':
        num = int(input('Введите число: '))
        if yn == 'нет':
            break
        else:
            print_welcome(num)
            yn = input('Продолжить? (да/нет): ')
    print('Конец!')