def main():
    """
    lambda возводит в 3ю степень переданный аргумент возвращает результат
    """
    print((lambda r: r ** 3)(2))
    pow3 = lambda r: r ** 3
    print(pow3(4))


if __name__ == '__main__':
    main()
