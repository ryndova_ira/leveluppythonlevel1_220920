def count_word(my_list, word):
    """
    Функция count_word.

    Принимает 2 аргумента:
        список слов my_list и
        строку word.

    Возвращает количество word в списке my_list.

    Если вместо списка передано что-то другое, то возвращать строку 'First arg must be list!'.
    Если вместо строки передано что-то другое, то возвращать строку 'Second arg must be str!'.
    Если список пуст, то возвращать строку 'Empty list!'.
    """

    if type(my_list) != list:
        return 'First arg must be list!'

    if type(word) != str:
        return 'Second arg must be str!'

    if len(my_list) == 0:
        return 'Empty list!'

    return my_list.count(word)


def main():
    yn = 'yes'
    while True:
        if yn == 'да' or yn == 'yes':
            my_list_str = input('Введите список слов через пробел: ')
            word = input('Введите искомое слово: ')
            my_list = my_list_str.split(' ')
            print(count_word(my_list, word))
            yn = input('Продолжить? (да/нет): ')
        elif yn == 'нет' or yn == 'no':
            break
        else:
            print('Некорректная команда!')
            yn = input('Продолжить? (да/нет): ')


if __name__ == '__main__':
    main()

print('Конец!')
