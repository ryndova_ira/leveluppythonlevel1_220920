def get_words_by_translation(ru_eng_dict, word):
    """
    Функция get_words_by_translation.

    Принимает 2 аргумента:
        ru-eng словарь содержащий {ru_word: [eng_1, eng_2 ...], ...}
        слово для поиска в словаре (eng).

    Возвращает список всех вариантов переводов (ru), если такое слово есть в словаре (eng),
    если нет, то ‘Can`t find English word: {word}’.

    Если вместо словаря передано что-то другое, то возвращать строку 'Dictionary must be dict!'.
    Если вместо строки для поиска передано что-то другое, то возвращать строку 'Word must be str!'.

    Если словарь пустой, то возвращать строку 'Dictionary is empty!'.
    Если строка для поиска пустая, то возвращать строку 'Word is empty!'.
    """
    if type(ru_eng_dict) != dict:
        return 'Dictionary must be dict!'

    if type(word) != str:
        return 'Word must be str!'

    if len(ru_eng_dict) == 0:
        return 'Dictionary is empty!'

    if len(word) == 0:
        return 'Word is empty!'

    ru_results = []
    for key, val in ru_eng_dict.items():
        if word in val:
            ru_results.append(key)

    if len(ru_results) == 0:
        return f"Can't find English word: {word}"

    return ru_results


def main():
    ru_eng = {'кошка': ['cat'], 'ручка': ['pen', 'hand'], 'человек': ['human', 'man', 'person'], 'мужчина': ['man']}
    print(get_words_by_translation(ru_eng, 'cat'))
    print(get_words_by_translation(ru_eng, 'hand'))
    print(get_words_by_translation(ru_eng, 'man'))
    print(get_words_by_translation(ru_eng, 'dog'))


if __name__ == '__main__':
    main()
