def decorator_maker_with_arguments(darg1, darg2):
    print("Создатель декораторов получил аргументы:", darg1, darg2)

    def my_decorator(func):
        print("Декоратор получил аргументы:",
              darg1, darg2)

        def wrapped(f_arg1, f_arg2):
            # Не перепутайте аргументы декораторов с аргументами функций!
            print("Обертка декорируемой функции имеет доступ ко всем аргументам \n"
                  "\t- и декоратора: {0} {1}\n\t- и функции: {2} {3}\n"
                  .format(darg1, darg2, f_arg1, f_arg2))
            return func(f_arg1, f_arg2)

        return wrapped

    return my_decorator


@decorator_maker_with_arguments("Дек1", "Дек2")
def decorated_function_with_arguments(function_arg1, function_arg2):
    print("Декорируемая функция знает только аргументы: {0} {1}"
          .format(function_arg1, function_arg2))


decorated_function_with_arguments("Функ1", "Функ2")
