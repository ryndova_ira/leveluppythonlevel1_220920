def count_num(my_list, num):
    """
    Функция count_num.

    Принимает 2 аргумента: список числами my_list и число num.

    Возвращает количество num в списке my_list.
    """
    return my_list.count(num)


def main():
    is_continue = ''
    while is_continue != 'yes':
        list_str = input("Введите список чисел через запятую: ")
        num_str = input("Введите число: ")
        list_str_splitted = list_str.split(',')
        print(count_num(list_str_splitted, num_str))
        is_continue = input("Хотите закончить (yes)? ")

    print("Спасибо за участие!")


if __name__ == '__main__':
    main()
