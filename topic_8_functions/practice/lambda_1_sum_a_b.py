def main():
    """
    lambda складывает аргументы a и b и выводит результат
    """
    (lambda a, b: print(a + b))(3, 4)
    sum_ab = lambda a, b: print(a + b)
    sum_ab(1, 2)


if __name__ == '__main__':
    main()
