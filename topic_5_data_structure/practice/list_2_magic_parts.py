def magic_parts(my_list):
    """
    Функция magic_parts.

    Принимает 1 аргумент: список my_list.

    Возвращает список, который состоит из
        [первые 2 элемента my_list]
        + [последний элемент my_list]
        + [количество элементов в списке my_list].

    Пример: входной список [1, 2, ‘aa’, ‘mm’], результат [1, 2, ‘mm’, 4].
    (* Проверить существуют ли "первые 2", если есть только 1, то берем его.
    Если список пустой - вернуть пустой список. Нумерация элементов начинается с 0.)
    """
    print(f'Список для обработки: {my_list}')

    if len(my_list) == 0:
        return []

    # result_list = [my_list[:2], my_list[-1], len(my_list)]    # - это альтернативный более короткий вариант
    result_list = []

    result_list.append(my_list[0])
    if len(my_list) >= 2:
        result_list.append(my_list[1])

    result_list.append(my_list[-1])
    result_list.append(len(my_list))
    return result_list  # return [my_list[:2], my_list[-1], len(my_list)]   # - это еще более короткая версия


def main():
    is_continue = ''
    while is_continue != 'yes':
        list_str = input("Введите список чисел через --: ")
        list_str_splitted = list_str.split('--')
        print(magic_parts(list_str_splitted))
        is_continue = input("Хотите закончить (yes)? ")

    print("Спасибо за участие!")


if __name__ == '__main__':
    main()
