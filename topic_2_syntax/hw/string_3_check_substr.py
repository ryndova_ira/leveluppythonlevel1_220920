def check_substr(str1, str2):
    """
    Функция check_substr.

    Принимает две строки.
    Если меньшая по длине строка содержится в большей, то возвращает True,
    иначе False.
    Если строки равны, то False.
    Если одна из строк пустая, то True.
    """
    str_diff = len(str1) - len(str2)
    if str_diff == 0:
        return False
    elif str_diff > 0:
        return str2 in str1
    else:
        return str1 in str2


def main():
    is_continue = ''
    while is_continue != 'yes':
        string1 = input("Введите строку 1: ")
        string2 = input("Введите строку 2: ")
        print(check_substr(string1, string2))
        is_continue = input("Хотите закончить (yes)? ")

    print("Спасибо за участие!")


if __name__ == '__main__':
    main()