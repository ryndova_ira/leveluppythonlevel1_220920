def list_to_dict(my_list, val):
    """
    Функция list_to_dict.

    Принимает 2 аргумента: список и значение для поиска.

    Возвращает словарь (из входного списка), в котором
        ключ = значение из списка,
        значение = (
            индекс этого элемента в списке,
            равно ли значение из списка искомому значению (True | False),
            количество элементов в словаре.
        )

    Если список пуст, то возвращать пустой словарь.

    Если вместо списка передано что-то другое, то возвращать строку 'First arg must be list!'.

    ВНИМАНИЕ: количество элементов в словаре не всегда равно количеству элементов в списке!

    ВНИМАНИЕ: при повторяющихся ключах в dict записывается значение последнего добавленного ключа.

    ВНИМАНИЕ: нумерация индексов начинается с 0!
    """

    if type(my_list) != list:
        return 'First arg must be list!'

    my_dict = {}
    len_list = len(set(my_list))
    for index, key in enumerate(my_list):
        my_dict[key] = (index, val == key, len_list)
    return my_dict


def main():
    yn = 'yes'
    while True:
        if yn == 'да' or yn == 'yes':
            my_list_str = input('Введите список повторяющихся элементов через пробел: ')
            word = input('Введите значение для поиска: ')
            my_list = my_list_str.split(' ')
            print(list_to_dict(my_list, word))
            yn = input('Продолжить? (да/нет): ')
        elif yn == 'нет' or yn == 'no':
            break
        else:
            print('Некорректная команда!')
            yn = input('Продолжить? (да/нет): ')


if __name__ == '__main__':
    main()

print('Конец!')
