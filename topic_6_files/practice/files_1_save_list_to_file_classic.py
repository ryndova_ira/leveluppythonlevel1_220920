def save_list_to_file_classic(file_name, my_list):
    """
    Функция save_list_to_file_classic.

    Принимает 2 аргумента:
        строка (название файла или полный путь к файлу),
        список (для сохранения).

    Сохраняет список в файл. Проверить, что записалось в файл (прочитать файл).
    """
    with open(file_name, "w") as my_file:
        my_file.write(str(my_list))


def main():
    lst = [1, 'qwertyu', True, [3, 4, 5]]
    file_name = 'test_save_list_classic.txt'
    save_list_to_file_classic(file_name, lst)

    with open(file_name, "r") as f:
        my_result = f.read()

    print(f'type: {type(my_result)}\nresult: {my_result}')


if __name__ == '__main__':
    main()
