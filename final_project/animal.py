from datetime import date


class Animal:
    count = 0

    def __init__(self, name, age, color):
        self.name = name
        self.age = age
        self.color = color

        # self.id = 1110 + Animal.count
        Animal.count += 1

    def __str__(self):
        return f'Имя: {self.name} | Возраст: {self.age} | Цвет: {self.color}'
