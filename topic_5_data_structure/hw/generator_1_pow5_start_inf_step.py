import itertools


def pow5_start_inf_step(start, step):
    """
    Функция pow5_start_inf_step.

    Принимает 2 аргумента: число start, step.

    Возвращает генератор-выражение состоящий из
    значений в 5 степени от start до бесконечности с шагом step.

    Пример: start=3, step=2 результат 3^5, 5^5, 7^5, 9^5 ... (infinity).

    Если start или step не являются int, то вернуть строку 'Start and Step must be int!'.
    """
    if type(start) != int or type(step) != int:
        return 'Start and Step must be int!'

    return (k ** 5 for k in itertools.count(start=start, step=step))


def main():
    yn = 'yes'
    while True:
        if yn == 'да' or yn == 'yes':
            my_start = int(input('Введите начальное значение: '))
            my_step = int(input('Введите шаг: '))
            my_repeat = int(input('Введите количество повторений: '))
            my_function = pow5_start_inf_step(my_start, my_step)
            print(next(my_function))
            print(next(my_function))
            print(next(my_function))
            for k in range(my_repeat):
                print(next(my_function))
            yn = input('Продолжить? Введите yes(да) или no(нет): ')
        elif yn == 'нет' or yn == 'no':
            break
        else:
            print('Некорректная команда!')
            yn = input('Продолжить? Введите yes(да) или no(нет): ')


if __name__ == '__main__':
    main()

print('Конец!')
