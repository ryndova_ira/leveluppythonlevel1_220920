По желанию можно добавлять любой другой функционал, 
расширять возможности классов и т.п. 

Можно фантазировать как угодно в рамках заданной темы. 
Чем более интересный получится проект, тем лучше 😉

---
# Завод
## Общее описание
- Состояние Завода (актуальный список товаров) необходимо хранить в файле 
и загружать/сохранять каждый раз при чтении/изменении.

- Список товаров представляет собой: категория-товар.

- Товары - это классы со своими свойствами
(у всех типов есть цена и размер упаковки; 
радио умеет играть музыку,
у матрасов имеется степень жесткости ...)

## Возможности (API):
- Просматривать/Изменять список товаров (со свойствами).
- Получить/Изменить название Завода.
- Получить тип(ы) товаров с самым большим / маленьким количеством на Заводе.
- Получить всех товаров из определенной категории.

## Тестирование
- Написать тесты для каждого класса (один файл *_tests = один класс)
    - Протестировать `__init__`
    - Протестировать `__str__`
    - Протестировать сохранение/загрузку файла
    - ...

## TeleBot
