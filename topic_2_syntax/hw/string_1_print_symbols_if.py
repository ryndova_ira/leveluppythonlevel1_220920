def print_symbols_if(string):
    """
    Функция print_symbols_if.

    Принимает строку.

    Если строка нулевой длины, то вывести строку "Empty string!".

    Если длина строки больше 5, то вывести первые три символа и последние три символа.
    Пример: string='123456789' => result='123789'

    Иначе вывести первый символ столько раз, какова длина строки.
    Пример: string='345' => result='333'
    """

    len_str = len(string)
    if len_str == 0:
        print("Empty string!")
    elif len_str > 5:
        left_chars = string[:3]
        right_chars = string[-3:]
        print(left_chars + right_chars)
    else:
        first_char = string[:1]
        print(len_str * first_char)


def main():
    yn = 'да'
    while True:
        if yn == 'да':
            st = input('Введите строку: ')
            print_symbols_if(st)
            yn = input('Продолжить? (да/нет): ')
        elif yn == 'нет':
            break
        else:
            print('Некорректная команда!')
            yn = input('Продолжить? (да/нет): ')


if __name__ == '__main__':
    main()

print('Конец!')
