def double_start_stop(start, stop):
    """
    Функция double_start_stop.

    Принимает 2 аргумента: числа start, stop.

    Возвращает словарь, в котором ключ - это значение от start до stop (не включая),
    а значение - удвоенных значение ключа.

    Пример: start=3, stop=6, результат {3: 6, 4: 8, 5: 10}.

    Если start или stop не являются int, то вернуть строку 'Start and Stop must be int!'.
    """
    if type(start) != int or type(stop) != int:
        return 'Start and Stop must be int!'

    return {k: k * 2 for k in range(start, stop)}


def main():
    print(double_start_stop(0, 5))
    print(double_start_stop(3, 6))
    my_result = double_start_stop(10, 20)
    print(f'my_result: {my_result}\nkeys: {my_result.keys()}\nvalues: {my_result.values()}')


if __name__ == '__main__':
    main()
