def pow_start_stop(start, stop):
    """
    Функция pow_start_stop.

    Принимает числа start, stop.

    Возвращает словарь, в котором
        ключ - это значение от start до stop (не включая)
        значение - это квадрат ключа.

    Пример: start=3, stop=6, результат {3: 9, 4: 16, 5: 25}.

    Если start или stop не являются int, то вернуть строку 'Start and Stop must be int!'.
    """

    if type(start) != int or type(stop) != int:
        return 'Start and Stop must be int!'

    return {k: k ** 2 for k in range(start, stop)}


def main():
    yn = 'yes'
    while True:
        if yn == 'да' or yn == 'yes':
            my_start = int(input('Введите начальное значение: '))
            my_stop = int(input('Введите конечное значение: '))
            print(pow_start_stop(my_start, my_stop))
            yn = input('Продолжить? Введите yes(да) или no(нет): ')
        elif yn == 'нет' or yn == 'no':
            break
        else:
            print('Некорректная команда!')
            yn = input('Продолжить? Введите yes(да) или no(нет): ')


if __name__ == '__main__':
    main()

print('Конец!')
