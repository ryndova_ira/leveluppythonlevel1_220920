class Goat:
    """
    Класс Goat.
    
    Поля:
        имя: name,
        возраст: age,
        сколько молока дает в день: milk_per_day.
    
    Методы:
        get_sound: вернуть строку 'Бе-бе-бе',
        __invert__: реверс строки с именем (например, была Маруся, а стала Ясурам). вернуть self
        __mul__: умножить milk_per_day на число. вернуть self
    """

    def __init__(self, name: str, age, milk_per_day):
        self.name = name
        self.age = age
        self.milk_per_day = milk_per_day

    def get_sound(self):
        return 'Бе-бе-бе'

    def __invert__(self):
        self.name = self.name[::-1].lower().title()
        return self

    def __mul__(self, other: int):
        self.milk_per_day *= other
        return self

