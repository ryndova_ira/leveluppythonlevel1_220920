def dict_pow2_start_stop_step_if_div3(start, stop, step):
    """
    Функция dict_pow2_start_stop_step_if_div3.

    Принимает 3 аргумента: число start, stop, step.

    Возвращает генератор-выражение состоящий из кортежа (аналог dict):
    (значение, значение в квадрате)
    при этом значения перебираются от start до stop (не включая) с шагом step
    только для чисел, которые делятся на 3 без остатка.

    Пример: start=0, stop=10, step=1 результат ((0, 0), (3, 9), (6, 36), (9, 81)).

    Если start или stop или step не являются int, то вернуть строку 'Start and Stop and Step must be int!'.
    Если step равен 0, то вернуть строку "Step can't be zero!"
    """
    if type(start) != int or type(stop) != int or type(step) != int:
        return 'Start and Stop and Step must be int!'

    if step == 0:
        return "Step can't be zero!"

    return ((k, k ** 2) for k in range(start, stop, step) if k % 3 == 0)


def main():
    yn = 'yes'
    while True:
        if yn == 'да' or yn == 'yes':
            my_start = int(input('Введите начальное значение: '))
            my_stop = int(input('Введите конечное значение: '))
            my_step = int(input('Введите шаг: '))
            my_function = dict_pow2_start_stop_step_if_div3(my_start, my_stop, my_step)
            print(dict(my_function))
            yn = input('Продолжить? Введите yes(да) или no(нет): ')
        elif yn == 'нет' or yn == 'no':
            break
        else:
            print('Некорректная команда!')
            yn = input('Продолжить? Введите yes(да) или no(нет): ')


if __name__ == '__main__':
    main()

print('Конец!')
