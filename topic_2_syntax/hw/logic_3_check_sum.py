def check_sum(num1, num2, num3):
    """
    Функция check_sum.

    Принимает 3 числа.
    Вернуть True, если можно взять какие-то два из них и в сумме получить третье, иначе False
    """
    return num1 + num2 == num3 or num1 + num3 == num2 or num2 + num3 == num1


if __name__ == '__main__':
    yn = 'да'
    while yn == 'да':
        n1 = int(input('Введите число 1: '))
        n2 = int(input('Введите число 2: '))
        n3 = int(input('Введите число 3: '))
        print(check_sum(n1, n2, n3))
        yn = input('Продолжить? (да/нет): ')
    print('Конец!')
