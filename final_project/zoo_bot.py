import os
import pickle

import telebot
from telebot import types

from final_project.animal import Animal
from final_project.toucan import Toucan
from final_project.unicorn import Unicorn
from final_project.zoo import Zoo

token = '1184152928:AAFvsnRFoML33KdHGu4xSx_GgQXAZrDueK0'
bot = telebot.TeleBot(token)

# {user_id: [zoo1, zoo2, ..]}
users_info = {}

# {user_id: current_zoo}
current_zoo = {}

hideBoard = types.ReplyKeyboardRemove()

bot_backup_filename = 'bot_backup.pkl'


def save_state():
    global users_info
    with open(bot_backup_filename, 'wb') as my_file:
        pickle.dump(users_info, my_file)


def load_state():
    global users_info
    if os.path.exists(bot_backup_filename) and os.path.isfile(bot_backup_filename):
        with open(bot_backup_filename, 'rb') as my_file:
            users_info = pickle.load(my_file)

    animal_count = 0
    unicorn_count = 0
    toucan_count = 0

    for user_zoo_list in users_info.values():
        for zoo in user_zoo_list:
            for animals in zoo.animals.values():
                for animal in animals:
                    if isinstance(animal, Animal):
                        animal_count += 1
                        Animal.count = animal_count

                    if isinstance(animal, Unicorn):
                        unicorn_count += 1
                        Unicorn.count = unicorn_count

                    if isinstance(animal, Toucan):
                        toucan_count += 1
                        Toucan.count = toucan_count


@bot.message_handler(commands=['info', 'help'])
def info(message):
    send_mess_info = f'Здравствуй, {message.from_user.first_name} {message.from_user.last_name}' \
                     f'Я твой личный менеджер зоопарков! \U0001F9DA' \
                     f'\nВы можете управлять мной, отправив эти команды:\n' \
                     f'\n/zoo_list - показать список ваших зоопарков' \
                     f'\n/zoo_add - добавить новый зоопарк' \
                     f'\n/help и /info - расскажет как запустить бота'
    bot.send_message(message.chat.id, send_mess_info, parse_mode='html')


@bot.message_handler(commands=['zoo_list'], content_types=['text'])
def zoo_list(message):
    global users_info

    user_zoo_list = users_info.get(message.from_user.id, None)
    if user_zoo_list is None:
        zoo_list_str = 'У вас еще нет ни одного зоопарка'
        bot.send_message(message.from_user.id, zoo_list_str)

    else:
        zoo_list_str = 'Выберите зоопарк, который хотите изменить'
        markup = types.InlineKeyboardMarkup()
        for index, user_zoo in enumerate(user_zoo_list):
            markup.add(types.InlineKeyboardButton(text=str(user_zoo), callback_data=index + 1))
        markup.add(types.InlineKeyboardButton(text='OK', callback_data=len(user_zoo_list) + 1))
        bot.send_message(message.from_user.id, zoo_list_str, reply_markup=markup)


@bot.callback_query_handler(func=lambda call: True)
def query_handler(call):
    bot.answer_callback_query(callback_query_id=call.id, text='информация')

    user_zoo_list = users_info.get(call.from_user.id, None)
    if str(len(user_zoo_list) + 1) == call.data:
        bot.send_message(call.message.chat.id, 'Ок, ожидаю следующей команды!')
    else:
        current_zoo[call.from_user.id] = user_zoo_list[int(call.data) - 1]
        bot.send_message(call.message.chat.id, f'Ок, выбран зоопарк:\n{current_zoo[call.from_user.id]}')
        bot.send_message(call.message.chat.id,
                         f'Животные в зоопарке:\n{current_zoo[call.from_user.id].get_all_animals()}')

        yes_no_cancel_keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True,
                                                           one_time_keyboard=True,
                                                           row_width=3)
        yes_no_cancel_keyboard.row("Добавить", "Удалить", "Отмена")

        bot.send_message(call.message.chat.id, 'Хотите добавить или удалить животное?',
                         reply_markup=yes_no_cancel_keyboard)

        bot.register_next_step_handler(call.message, add_animals_yes_no_cancel)


def add_animals_yes_no_cancel(message):
    if message.text.lower() == 'добавить':
        animals_keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=2)
        animals_keyboard.row("Единорог", "Тукан")
        bot.send_message(message.from_user.id, "Выбери вид животного", reply_markup=animals_keyboard)
        bot.register_next_step_handler(message, add_animal)
    elif message.text.lower() == 'удалить':
        animals_keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
        current_user_animals = current_zoo[message.from_user.id].animals

        for animals in current_user_animals.values():
            for animal in animals:
                animals_keyboard.row(str(animal))
        bot.send_message(message.from_user.id, "Выбери животное", reply_markup=animals_keyboard)
        bot.register_next_step_handler(message, del_animal)
    else:
        bot.send_message(message.from_user.id, "Ок, скажешь, когда будешь готов!", reply_markup=hideBoard)


def add_animal(message):
    if message.text.lower() == 'единорог':
        bot.register_next_step_handler(message, add_unicorn)

        bot.send_message(message.from_user.id,
                         'Введите имя, возраст, цвет, цвет фейерверка нового единорога '
                         '(через запятую с пробелом)', reply_markup=hideBoard)
    elif message.text.lower() == 'тукан':
        bot.register_next_step_handler(message, add_toucan)

        bot.send_message(message.from_user.id,
                         'Введите имя, возраст, цвет, размах крыла (метры), высота полета (метры) '
                         '(через запятую с пробелом)', reply_markup=hideBoard)


def add_unicorn(message):
    name, age, color, firework_color = message.text.split(', ')
    unicorn = Unicorn(name=name, age=int(age), color=color, firework_color=firework_color)
    current_zoo[message.from_user.id].add_animal('Лошади', unicorn)
    bot.send_message(message.from_user.id, f"Единорог добавлен!\n{unicorn}", reply_markup=hideBoard)

    save_state()


def add_toucan(message):
    name, age, color, wing_len, fly_height_m = message.text.split(', ')
    toucan = Toucan(name, int(age), color, int(wing_len), int(fly_height_m))
    current_zoo[message.from_user.id].add_animal('Птицы', toucan)
    bot.send_message(message.from_user.id, f"Тукан добавлен!\n{toucan}", reply_markup=hideBoard)

    save_state()


def del_animal(message):
    animal_str = message.text
    animal_str_short = animal_str.replace(' Имя: ', '') \
        .replace('Возраст: ', '') \
        .replace('Цвет: ', '') \
        .replace('Цвет звездочек: ', '') \
        .replace('Размах крыла: ', '') \
        .replace('Высота полета: ', '')
    animal_type, animal_feats = animal_str_short.split(':')
    if animal_type == 'Unicorn':
        name, age, color, firework_color = animal_feats.split(' | ')
        del_result = current_zoo[message.from_user.id].del_animal_by_feat(animal_type=animal_type,
                                                                          name=name,
                                                                          age=int(age),
                                                                          color=color,
                                                                          firework_color=firework_color)

        bot.send_message(message.from_user.id, f"Единорог {'удален' if del_result else 'не найден'}!",
                         reply_markup=hideBoard)
    elif animal_type == 'Toucan':
        name, age, color, wing_len, fly_height_m = animal_feats.split(' | ')
        del_result = current_zoo[message.from_user.id].del_animal_by_feat(animal_type=animal_type,
                                                                          name=name,
                                                                          age=int(age),
                                                                          color=color,
                                                                          wing_len=int(wing_len),
                                                                          fly_height_m=int(fly_height_m))
        bot.send_message(message.from_user.id, f"Тукан {'удален' if del_result else 'не найден'}!",
                         reply_markup=hideBoard)

    save_state()


@bot.message_handler(commands=['zoo_add'], content_types=['text'])
def zoo_add(message):
    bot.send_message(message.from_user.id, "Введите название нового зоопарка:")

    bot.register_next_step_handler(message, get_zoo_title)


def get_zoo_title(message):
    global users_info
    if users_info.get(message.from_user.id, None) is None:
        users_info[message.from_user.id] = []

    new_zoo = Zoo(title=message.text,
                  owner=f'{message.from_user.first_name} {message.from_user.last_name}',
                  animals_backup_filename=f'user_{message.from_user.id}.pkl')
    users_info[message.from_user.id].append(new_zoo)

    save_state()

    bot.send_message(message.from_user.id, f'Зоопарк добавлен:\n{new_zoo.info_str}')


if __name__ == '__main__':
    print('Starting bot...')
    load_state()
    bot.polling(none_stop=True, interval=0)
