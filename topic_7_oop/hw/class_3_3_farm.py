from topic_7_oop.hw.class_3_1_chicken import Chicken
from topic_7_oop.hw.class_3_2_goat import Goat


class Farm:
    """
    Класс Farm.

    Поля:
        животные (list из произвольного количества Goat и Chicken): animals
                            (вначале список пуст, потом добавялем животных методами append и extend самостоятельно),
        наименование фермы: name,
        имя владельца фермы: owner.

    Методы:
        get_goat_count: вернуть количество коз на ферме (*Подсказка isinstance или type == Goat),
        get_chicken_count: вернуть количество куриц на ферме,
        get_animals_count: вернуть количество животных на ферме,
        get_milk_count: вернуть сколько молока можно получить в день,
        get_eggs_count: вернуть сколько яиц можно получить в день.
    """

    def __init__(self, name, owner):
        self.name = name
        self.owner = owner
        self.animals = []

    def get_goat_count(self):
        count = 0
        for animal in self.animals:
            if type(animal) == Goat:
                count += 1
        return count

    def get_chicken_count(self):
        count = 0
        for animal in self.animals:
            if type(animal) == Chicken:
                count += 1
        return count

    def get_animals_count(self):
        return len(self.animals)

    def get_milk_count(self):
        count = 0
        for animal in self.animals:
            if type(animal) == Goat:
                count += animal.milk_per_day
        return count

    def get_eggs_count(self):
        count = 0
        for animal in self.animals:
            if type(animal) == Chicken:
                count += animal.eggs_per_day
        return count

