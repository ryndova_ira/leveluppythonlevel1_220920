def count_odd_num(number):
    """
    Функция count_odd_num.

    Принимает натуральное число (целое число > 0).
    Верните количество нечетных цифр в этом числе.
    Если число меньше или равно 0, то вернуть "Must be > 0!".
    Если число не целое (не int, а другой тип данных), то вернуть "Must be int!".
    """

    if type(number) != int:
        return "Must be int!"
    elif number <= 0:
        return "Must be > 0!"

    count = 0
    for char in str(number):
        if int(char) % 2 == 1:
            count += 1
    return count


if __name__ == '__main__':
    while True:
        num_str = input('Введите натуральное число (или 0 - для завершения: ')
        num = int(num_str)
        if num == 0:
            break
        else:
            print(count_odd_num(num))
    print('Конец!')
