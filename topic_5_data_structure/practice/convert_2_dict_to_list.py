def dict_to_list(my_dict):
    """
    Функция dict_to_list.

    Принимает 1 аргумент: словарь.

    Возвращает список: [
        список ключей,
        список значений,
        количество элементов в списке ключей в степени 3,
        количество уникальных элементов в списке значений,
        хотя бы один ключ равен одному из значений (True | False).
    ]

    Если вместо словаря передано что-то другое, то возвращать строку 'Must be dict!'.

    Если dict пуст, то возвращать ([], [], 0, 0, False).
    """

    if type(my_dict) != dict:
        return 'Must be dict!'

    result_list = []

    values = list(my_dict.values())

    result_list.append(list(my_dict.keys()))
    result_list.append(values)
    result_list.append(len(my_dict.keys()) ** 3)
    result_list.append(len(set(values)))

    for key in my_dict.keys():
        if key in values:
            result_list.append(True)
            break
    else:
        result_list.append(False)

    return result_list


def main():
    test_dict = {'rrr': '3-5-33', 'tre': '083-7363-33', 'qqq': '83737', 'rewsd': 'qqq'}
    res_list = dict_to_list(test_dict)

    print(res_list)


if __name__ == '__main__':
    main()
