def get_numbers_by_name(my_dict, name):
    """
    Функция get_numbers_by_name.

    Принимает 2 аргумента:
        словарь содержащий {имя: [телефон1, телефон2], ...},
        слово (имя) для поиска в словаре.

    Если вместо словаря передано что-то другое, то возвращать строку "Dictionary must be dict!".
    Если вместо строки для поиска передано что-то другое, то возвращать строку "Name must be str!".

    Если словарь пустой, то возвращать строку "Dictionary is empty!".
    Если строка для поиска пустая, то возвращать строку "Name is empty!".

    Вернуть все телефоны по имени (list), если такое имя есть в словаре,
    если нет, то "Can't find name: {name}".
    """

    if type(my_dict) != dict:
        return "Dictionary must be dict!"

    if type(name) != str:
        return "Name must be str!"

    if len(my_dict) == 0:
        return "Dictionary is empty!"

    if len(name) == 0:
        return "Name is empty!"

    return my_dict.get(name, f"Can't find name: {name}")


def main():
    test_dict = {'rrr': '3-5-33', 'tre': '083-7363-33'}
    print(get_numbers_by_name(test_dict, 'rrr'))
    print(get_numbers_by_name(test_dict, 'ttt'))


if __name__ == '__main__':
    main()
