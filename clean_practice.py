import os


def find_all(string, substring):
    """
    Yields all the positions of
    the pattern p in the string s.
    """

    result_idxs = []
    idx = string.find(substring)
    while idx != -1:
        result_idxs.append(idx)
        idx = string.find(substring, idx + 1)

    return result_idxs

def remove_str_before_after_idxs(string: str, idx_start: int, idx_stop: int):
    pass

def get_only_task_comment(string: str):
    parts = string.split('"""')
    if len(parts) == 3:
        pass
        return '"""' + parts[1].replace('    ', '') + '"""'
    else:
        pass
        return parts[0]

def clean_file_with_solution(file_path):
    with open(file_path, "r") as f:
        text = f.read()
        cleaned_string = get_only_task_comment(text)

    with open(file_path, "w") as f:
        f.write(cleaned_string)


folders_with_solutions = []
folder = []
for i in os.walk('.'):
    folder.append(i)
    if 'hw' in i[0] or 'practice' in i[0]:
        folders_with_solutions.append(i)

# for address, dirs, files in folder:
#     print(address, dirs, files)

folders_with_solutions.sort()
for address, dirs, files in folders_with_solutions:
    print(address, dirs, files)
    for file_path in files:
        if '.pyc' not in file_path and '__init__' not in file_path:
            clean_file_with_solution(os.path.join(address, file_path))

pass
