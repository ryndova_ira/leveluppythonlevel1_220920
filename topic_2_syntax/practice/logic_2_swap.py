def swap(first_param, second_param):
    """
    Функция swap.

    Принимает 2 аргумента (first_param, second_param) с некоторыми значениями.
    Поменять местами значения этих переменных и вывести на экран таким образом "first_param = 3 | second_param = True".
    """
    first_param, second_param = second_param, first_param
    print(f"first_param = {first_param} | second_param = {second_param}")


def main():
    swap(1, 5)
    swap('hhh', 999)
    swap('opi', True)


if __name__ == '__main__':
    main()
