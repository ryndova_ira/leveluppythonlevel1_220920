from itertools import zip_longest


def zip_name_mark(name_lst, mark_lst):
    """
    Функция zip_name_mark.

    Принимает 2 аргумента: список с именами и список с баллами.

    Возвращает список (list) с парами значений из каждого аргумента, если один список больше другого,
    то заполнить недостающие элементы строкой "!!!".

    Подсказка: zip_longest.

    Если вместо списков передано что-то другое, то возвращать строку 'Both args must be list!'.

    Если список (хотя бы один) пуст, то возвращать строку 'Empty list!'.

    Если в списке с именами есть элементы НЕ str, то возвращать строку 'Marks must be str!'

    Если в списке с оценками есть элементы НЕ int (0 <= mark <= 100),
    то возвращать строку 'Names must be int (0 to 100)!'
    """

    if type(name_lst) != list or type(mark_lst) != list:
        return 'Both args must be list!'

    if len(name_lst) == 0 or len(mark_lst) == 0:
        return 'Empty list!'

    for name in name_lst:
        if type(name) != str:
            return 'Names must be str!'

    for mark in mark_lst:
        if type(mark) != int or (mark < 0 or mark > 100):
            return 'Marks must be int (0 to 100)!'

    return list(zip_longest(name_lst, mark_lst, fillvalue='!!!'))


def main():
    nlst = ['вася', "петя", "маша"]
    mlst = [2, 5, 5]
    print(zip_name_mark(nlst, mlst))


if __name__ == '__main__':
    main()
