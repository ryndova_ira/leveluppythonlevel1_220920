def print_hi(n):
    """
    Функция print_hi.

    Принимает число n.
    Выведите на экран n раз фразу "Hi, friend!"
    Пример: n=3, тогда в результате "Hi, friend!Hi, friend!Hi, friend!"
    """
    print(n * "Hi, friend!")


if __name__ == '__main__':
    yn = 'да'
    while yn == 'да':
        num = int(input('Введите число: '))
        if yn == 'нет':
            break
        else:
            print_hi(num)
            yn = input('Продолжить? (да/нет): ')
    print('Конец!')
