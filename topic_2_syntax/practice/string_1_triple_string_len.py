def triple_string_len(my_str):
    """
    Функция triple_sting_print_len.

    Принимает строку my_str.
    Вернуть эту строку в таком виде: "<my_str>, <my_str>? <my_str>! len=<len(my_str)> :)".

    Если строка пустая, то вернуть None
    """

    if len(my_str) == 0:
        return None

    # return "{0}, {0}? {0}! len={1} :)".format(my_str, len(my_str))
    # return "{s}, {s}? {s}! len={len_s} :)".format(s=my_str, len_s=len(my_str))
    return f"{my_str}, {my_str}? {my_str}! len={len(my_str)} :)"


def main():
    is_continue = ''
    while is_continue != 'yes':
        string = input("Введите строку: ")
        print(triple_string_len(string))
        is_continue = input("Хотите закончить (yes)? ")

    print("Спасибо за участие!")


if __name__ == '__main__':
    main()
