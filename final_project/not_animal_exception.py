class NotAnimalException(Exception):
    """
    Raised when the input value isn't subclass/class Animal
    """
    pass
