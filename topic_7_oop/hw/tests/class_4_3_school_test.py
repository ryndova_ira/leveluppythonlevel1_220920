import pytest

from topic_7_oop.hw.class_4_1_pupil import Pupil
from topic_7_oop.hw.class_4_2_worker import Worker
from topic_7_oop.hw.class_4_3_school import School


def test():
    pupil_1 = Pupil(name='Петя',
                    age=15,
                    marks={
                        'math': [3, 3, 3],
                        'history': [4, 4, 4],
                        'english': [5, 5, 5]
                    })
    pupil_2 = Pupil(name='Маша',
                    age=15,
                    marks={
                        'math': [4, 4, 4],
                        'history': [4, 4, 4],
                        'english': [4, 4, 4]
                    })

    pupil_3 = Pupil(name='Маша',
                    age=13,
                    marks={
                        'math': [5, 5, 5],
                        'history': [5, 5, 5],
                        'english': [5, 5, 5]
                    })

    worker_1 = Worker(name='Миша',
                      salary=86324.50,
                      position="Учитель математики")

    worker_2 = Worker(name='Лена',
                      salary=56324.50,
                      position="Повар")

    worker_3 = Worker(name='Василиса',
                      salary=56324.50,
                      position="Учитель математики")

    school = School(number=555,
                    people=[
                        pupil_1,
                        pupil_2,
                        pupil_3,

                        worker_1,
                        worker_2,
                        worker_3
                    ]
                    )

    assert school.get_avg_mark() == pytest.approx(4.3, 0.1)

    assert school.get_avg_salary() == pytest.approx(66324.5, 0.001)

    assert school.get_worker_count() == 3

    assert school.get_pupil_count() == 3

    assert sorted(school.get_pupil_names()) == sorted(['Петя', 'Маша', 'Маша'])

    assert sorted(school.get_unique_worker_positions()) == sorted(["Учитель математики", "Повар"])

    assert school.get_max_pupil_age() == 15

    assert school.get_min_worker_salary() == pytest.approx(56324.50, 0.0001)

    assert sorted(school.get_min_salary_worker_names()) == sorted(['Василиса', 'Лена'])
