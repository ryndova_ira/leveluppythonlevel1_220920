def len_count_first(my_tuple, word):
    """
    Функция len_count_first.

    Принимает 2 аргумента: кортеж my_tuple и строку word.

    Возвращает кортеж состоящий из
        длины кортежа,
        количества word в кортеже my_tuple,
        первого элемента кортежа.

    Пример:
        my_tuple=('55', 'aa', '66')
        word = '66'
        результат (3, 1, '55').

    Если вместо tuple передано что-то другое, то возвращать строку 'First arg must be tuple!'.
    Если вместо строки передано что-то другое, то возвращать строку 'Second arg must be str!'.
    Если tuple пуст, то возвращать строку 'Empty tuple!'.
    """

    if type(my_tuple) != tuple:
        return 'First arg must be tuple!'

    if type(word) != str:
        return 'Second arg must be str!'

    if len(my_tuple) == 0:
        return 'Empty tuple!'

    new_tuple = (len(my_tuple), my_tuple.count(word), my_tuple[0])
    return new_tuple


def main():
    yn = 'yes'
    while True:
        if yn == 'да' or yn == 'yes':
            my_tuple_str = input('Введите список элементов через пробел: ')
            my_list = my_tuple_str.split(' ')
            my_tuple = tuple(my_list)
            num = int(input('Введите число: '))
            print(len_count_first(my_tuple, num))
            yn = input('Продолжить? (да/нет): ')
        elif yn == 'нет' or yn == 'no':
            break
        else:
            print('Некорректная команда!')
            yn = input('Продолжить? (да/нет): ')


if __name__ == '__main__':
    main()

print('Конец!')
