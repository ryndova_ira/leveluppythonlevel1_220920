from datetime import date

from final_project.animal import Animal


class Unicorn(Animal):
    count = 0

    def __init__(self, name, age, color, firework_color):
        super().__init__(name, age, color)
        self.firework_color = firework_color

        Unicorn.count += 1

    @classmethod
    def from_birth_year(cls, name, birth_year, color, firework_color):
        return cls(name, date.today().year - birth_year, color, firework_color)

    def start_firework(self):
        return f'Единорог {self.name} запустил звездочки {self.firework_color} цвета!!!'

    def __str__(self):
        return f'Unicorn: {super().__str__()} | Цвет звездочек: {self.firework_color}'

    def __eq__(self, other):
        return self.name == other['name'] \
               and self.age == other['age'] \
               and self.color == other['color'] \
               and self.firework_color == other['firework_color']


def main():
    unicorn_1 = Unicorn('Вася', 5, 'белый', "фиолетового")
    unicorn_1.start_firework()
    print(f'unicorn_count: {Unicorn.count}')
    unicorn_2 = Unicorn('Маша', 1, 'синий', "золотого")
    print(f'unicorn_count: {Unicorn.count}')

    print(f'count: {Unicorn.count}')


if __name__ == '__main__':
    main()
