def pow_odd(n):
    """
    Функция pow_odd.

    Принимает число n.

    Возвращает список длиной n, состоящий из квадратов нечетных чисел в диапазоне от 0 до n (не включая).

    Пример: n = 7, четные числа [1, 3, 5], результат [1, 9, 25]

    Если n не является int, то вернуть строку 'Must be int!'.
    """
    if type(n) != int:
        return 'Must be int!'

    return [k ** 2 for k in range(n) if k % 2 == 1]


def main():
    yn = 'yes'
    while True:
        if yn == 'да' or yn == 'yes':
            long = int(input('Введите длину списка: '))
            print(pow_odd(long))
            yn = input('Продолжить? Введите yes(да) или no(нет): ')
        elif yn == 'нет' or yn == 'no':
            break
        else:
            print('Некорректная команда!')
            yn = input('Продолжить? Введите yes(да) или no(нет): ')


if __name__ == '__main__':
    main()

print('Конец!')
