def list_to_str(my_list, sep):
    """
    Функция list_to_str.

    Принимает 2 аргумента: список и разделитель (строка).

    Возвращает (строку полученную разделением элементов списка разделителем,
                количество разделителей в получившейся строке в квадрате).

    Если вместо списка передано что-то другое, то возвращать строку 'First arg must be list!'.

    Если разделитель не является строкой, то возвращать строку 'Second arg must be str!'.

    Если список пуст, то возвращать пустой tuple().

    ВНИМАНИЕ: в списке могут быть элементы любого типа (нужно конвертировать в строку).
    """

    if type(my_list) != list:
        return 'First arg must be list!'

    if type(sep) != str:
        return 'Second arg must be str!'

    if len(my_list) == 0:
        return tuple()

    len_my_list = len(my_list)

    new_str = sep.join([str(elem) for elem in my_list])
    return new_str, (len_my_list - 1) ** 2


def main():
    yn = 'yes'
    while True:
        if yn == 'да' or yn == 'yes':
            my_list_str = input('Введите список слов через пробел: ')
            znak = input('Введите разделитель для "склейки" списка в строку: ')
            my_list = my_list_str.split(' ')
            print(list_to_str(my_list, znak))
            yn = input('Продолжить? (да/нет): ')
        elif yn == 'нет' or yn == 'no':
            break
        else:
            print('Некорректная команда!')
            yn = input('Продолжить? (да/нет): ')


if __name__ == '__main__':
    main()

print('Конец!')
