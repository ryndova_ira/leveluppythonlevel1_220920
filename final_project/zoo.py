import pickle
import os.path

from final_project.animal import Animal
from final_project.unicorn import Unicorn
from final_project.toucan import Toucan
from final_project.not_animal_exception import NotAnimalException


class Zoo:
    def __init__(self, title="Обычный зоопарк",
                 owner='Государство',
                 animals_backup_filename='animals.pkl',
                 should_load_backup=True):
        self.title = title
        self.owner = owner
        # {
        #   'Лошади': [Unicorn_1, Unicorn_2, Pegasus_1],
        #   'Птицы': [Toucan_1, Parrot_1, Toucan_2]
        # }
        self.animals = {}

        self.animals_backup_filename = animals_backup_filename

        if should_load_backup and os.path.exists(self.animals_backup_filename) and os.path.isfile(
                self.animals_backup_filename):
            self._load_animals()

    @property
    def info_str(self):
        """
        Информация о зоопарке
        :return: Строка с названием и владельцем
        """
        return f'{"*" * 100}\nНазвание: {self.title}\nВладелец: {self.owner}\n{"*" * 100}'

    def __str__(self):
        return f'Название: {self.title}\nВладелец: {self.owner}'

    def _load_animals(self):
        with open(self.animals_backup_filename, 'rb') as my_file:
            self.animals = pickle.load(my_file)

            animal_count = 0
            unicorn_count = 0
            toucan_count = 0
            for animals in self.animals.values():
                for animal in animals:
                    if isinstance(animal, Animal):
                        animal_count += 1
                        Animal.count = animal_count

                    if isinstance(animal, Unicorn):
                        unicorn_count += 1
                        Unicorn.count = unicorn_count

                    if isinstance(animal, Toucan):
                        toucan_count += 1
                        Toucan.count = toucan_count

    def _save_animals(self):
        with open(self.animals_backup_filename, 'wb') as my_file:
            pickle.dump(self.animals, my_file)

    def add_animal(self, category, animal):
        if not isinstance(animal, Animal):
            raise NotAnimalException(f"Object {animal} (type = {type(animal)}) isn't Animal instance")

        if category not in self.animals.keys():
            self.animals[category] = []

        self.animals[category].append(animal)

        self._save_animals()

    def add_animals(self, category, *animals):
        if category not in self.animals.keys():
            self.animals[category] = []

        for animal in animals:
            if not isinstance(animal, Animal):
                raise NotAnimalException(f"Object {animal} (type = {type(animal)}) isn't Animal instance")

            self.animals[category].append(animal)

        self._save_animals()

    def get_all_animals(self):
        result_string = ''
        for category, animals in self.animals.items():
            result_string += category + ':\n'

            for animal in animals:
                result_string += f'\t{str(animal)}\n'

            result_string += '\n'

        return result_string

    def get_title(self):
        return f'*** {self.title} ***'

    def get_animals_by_category(self, category):
        return self.animals.get(category)

    def get_animal_with_min_count(self):
        if len(self.animals) == 0:
            return []

        animal_types = {}  # {'Unicorn': 4}
        for animals in self.animals.values():
            for animal in animals:
                animal_types[animal.__class__.__name__] = animal.count

        final_elements = []

        min_n = min(animal_types.values())
        for t, n in animal_types.items():
            if n <= min_n:
                final_elements.append(t)

        return final_elements

    def del_animal_by_feat(self, animal_type, **animal_feats):
        is_found = False
        category_found = ''
        animal_index_found = None
        for category, animals in self.animals.items():
            for idx, animal in enumerate(animals):
                if animal == animal_feats:
                    category_found = category
                    animal_index_found = idx
                    is_found = True
                    break

            if is_found:
                break

        if is_found:
            del self.animals[category_found][animal_index_found]
            return True
        return False


def demos():
    my_zoo = Zoo(title='Самый лучший зоопарк')
    print(my_zoo.get_title())

    my_zoo.title = 'Самый волшебный зоопарк'
    print(my_zoo.get_title())

    print('-' * 50)

    unicorn_1 = Unicorn(name='Вася', color='белый', firework_color="фиолетового", age=5)
    unicorn_2 = Unicorn('Маша', 1, 'синий', "золотого")
    my_zoo.add_animal('Лошади', unicorn_1)
    my_zoo.add_animal('Лошади', unicorn_2)

    toucan_1 = Toucan('Лиза', 2, 'черный', 0.75, 100)
    toucan_2 = Toucan('Петя', 6, 'черный', 1.5, 200)
    toucan_3 = Toucan('Лена', 6, 'черный', 1.5, 200)
    my_zoo.add_animal('Птицы', toucan_1)
    my_zoo.add_animal('Птицы', toucan_2)
    my_zoo.add_animal('Птицы', toucan_3)

    my_zoo.get_all_animals()

    print('-' * 50)

    min_count = my_zoo.get_animal_with_min_count()
    print(f'min count: {min_count}')

    print('-' * 50)

    defaut_zoo = Zoo()
    print(defaut_zoo.get_title())

    defaut_zoo.add_animals('Лошади', unicorn_1, unicorn_2)

    defaut_zoo.add_animals('Птицы', toucan_1, toucan_2, toucan_3)
    defaut_zoo.get_all_animals()


def demos_with_exceptions():
    my_new_zoo = Zoo('Мой новый зоопарк', animals_backup_filename='animals_new.pkl')
    unicorn_0 = Unicorn('Зоя', 6, 'синий', "золотого")
    unicorn_1 = Unicorn(name='Вася', color='белый', firework_color="фиолетового", age=5)
    unicorn_2 = Unicorn('Маша', 1, 'синий', "золотого")
    my_new_zoo.add_animals('Лошади', unicorn_0, unicorn_1, unicorn_2)

    toucan_1 = Toucan('Лиза', 2, 'черный', 0.75, 100)
    toucan_2 = Toucan('Петя', 6, 'черный', 1.5, 200)
    toucan_3 = Toucan('Лена', 6, 'черный', 1.5, 200)
    my_new_zoo.add_animals('Птицы', toucan_1, toucan_2, toucan_3)

    my_new_zoo.get_all_animals()

    print('=' * 100)

    try:
        my_new_zoo.add_animal('Set', {1, 3, 4})
    except NotAnimalException as exp:
        print(f'Получена NotAnimalException ошибка: {exp}')
    except Exception as exp:
        print(f'Получена неизвестная ошибка: {exp}')
    else:
        print('Обновленный список:')
        my_new_zoo.get_all_animals()
    finally:
        print('Спасибо!')

    try:
        my_new_zoo.add_animal([1, 2, 3], toucan_1)
    except NotAnimalException as exp:
        print(f'Получена NotAnimalException ошибка: {exp}')
    except Exception as exp:
        print(f'Получена неизвестная ошибка {exp}')
    else:
        print('Обновленный список:')
        my_new_zoo.get_all_animals()
    finally:
        print('Спасибо!')

    try:
        my_new_zoo.add_animal('Птицы', toucan_1)
    except NotAnimalException as exp:
        print(f'Получена NotAnimalException ошибка: {exp}')
    except Exception as exp:
        print(f'Получена неизвестная ошибка {exp}')
    else:
        print('Обновленный список:')
        my_new_zoo.get_all_animals()
    finally:
        print('Спасибо!')


def main():
    try:
        my_new_zoo = Zoo('Мой новый зоопарк', animals_backup_filename='animals_new.pkl')
        unicorn_0 = Unicorn.from_birth_year('Зоя', 2015, 'синий', "золотого")
        unicorn_1 = Unicorn.from_birth_year(name='Вася',
                                            color='белый',
                                            firework_color="фиолетового",
                                            birth_year=2019)
        unicorn_2 = Unicorn('Маша', 2018, 'синий', "золотого")
        my_new_zoo.add_animals('Лошади', unicorn_0, unicorn_1, unicorn_2)

        toucan_1 = Toucan.from_birth_year('Лиза', 2018, 'черный', 0.75, 1)
        toucan_2 = Toucan.from_birth_year('Петя', 2010, 'черный', 1.5, 0.5)
        toucan_3 = Toucan.from_birth_year('Лена', 2012, 'черный', 1.5, 200)
        my_new_zoo.add_animals('Птицы', toucan_1, toucan_2, toucan_3)

        print(f'Can toucan_1 fly? {Toucan.can_fly(toucan_1.fly_height_m)}')
        print(f'Can toucan_2 fly? {Toucan.can_fly(toucan_2.fly_height_m)}')
        print(f'Can toucan_3 fly? {Toucan.can_fly(toucan_3.fly_height_m)}')

        print(my_new_zoo.info_str)

    except NotAnimalException as exp:
        print(f'Получена NotAnimalException ошибка: {exp}')
    except Exception as exp:
        print(f'Получена неизвестная ошибка {exp}')
    else:
        print('Обновленный список:')
        print(my_new_zoo.get_all_animals())
    finally:
        print('Спасибо!')


if __name__ == '__main__':
    main()
