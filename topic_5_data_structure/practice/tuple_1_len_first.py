def len_first(my_tuple):
    """
    Функция len_first.

    Принимает 1 аргумент: кортеж my_tuple.

    Возвращает кортеж состоящий из длины кортежа и первого элемента кортежа.

    Пример: (‘55’, ‘aa’, 66), результат (3, ‘55’).
    """

    print(f"Мой кортеж: {my_tuple}")

    return len(my_tuple), my_tuple[0] if len(my_tuple) > 0 else None


def main():
    is_continue = ''
    while is_continue != 'yes':
        list_str = input("Введите список чисел через .: ")
        list_str_splitted = tuple(list_str.split('.')) if len(list_str) > 0 else ()
        print(len_first(list_str_splitted))
        is_continue = input("Хотите закончить (yes)? ")

    print("Спасибо за участие!")


if __name__ == '__main__':
    main()
