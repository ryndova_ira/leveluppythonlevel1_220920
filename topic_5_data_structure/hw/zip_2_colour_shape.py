def zip_colour_shape(colour_list, shape_tuple):
    """
    Функция zip_colour_shape.

    Принимает 2 аргумента: список с цветами (зеленый, красный) и кортеж с формами (квадрат, круг).

    Возвращает список с парами значений из каждого аргумента.

    Если вместо list передано что-то другое, то возвращать строку 'First arg must be list!'.
    Если вместо tuple передано что-то другое, то возвращать строку 'Second arg must be tuple!'.

    Если list пуст, то возвращать строку 'Empty list!'.
    Если tuple пуст, то возвращать строку 'Empty tuple!'.

    Если list и tuple различного размера, обрезаем до минимального (стандартный zip).
    """
    
    if type(colour_list) != list:
        return 'First arg must be list!'

    if type(shape_tuple) != tuple:
        return 'Second arg must be tuple!'

    if len(colour_list) == 0:
        return 'Empty list!'

    if len(shape_tuple) == 0:
        return 'Empty tuple!'
    
    return list(zip(colour_list, shape_tuple))


def main():
    yn = 'yes'
    while True:
        if yn == 'да' or yn == 'yes':
            my_list_colour_str = input('Введите цвета через пробел: ')
            if len(my_list_colour_str) != 0:
                my_list_colour = my_list_colour_str.split(' ')
            else:
                my_list_colour = ()
            my_tuple_form_str = input('Введите формы фигур через пробел: ')
            if len(my_tuple_form_str) != 0:
                my_tuple_form_list = my_tuple_form_str.split(' ')
            else:
                my_tuple_form_list = ()
            my_tuple_form = tuple(my_tuple_form_list)
            print(zip_colour_shape(my_list_colour, my_tuple_form))
            yn = input('Продолжить? Введите yes(да) или no(нет): ')
        elif yn == 'нет' or yn == 'no':
            break
        else:
            print('Некорректная команда!')
            yn = input('Продолжить? Введите yes(да) или no(нет): ')


if __name__ == '__main__':
    main()

print('Конец!')
