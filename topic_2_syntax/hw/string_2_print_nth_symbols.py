def print_nth_symbols(string, n):
    """
    Функция print_nth_symbols.

    Принимает строку и натуральное число n (целое число > 0).
    Вывести символы с индексом n, n*2, n*3 и так далее.
    Пример: string='123456789qwertyuiop', n = 2 => result='3579wryip'

    Если число меньше или равно 0, то вывести строку 'Must be > 0!'.
    Если тип n не int, то вывести строку 'Must be int!'.

    Если n больше длины строки, то вывести пустую строку.
    """
    if type(n) != int:
        print('Must be int!')
    elif n <= 0:
        print('Must be > 0!')
    else:
        # При n=2: 2(n), 4(n*2), 6(n*3), 8(n*4), 10(n*5) ...
        print(string[n::n])


def main():
    is_continue = ''
    while is_continue != 'yes':
        string = input("Введите строку: ")
        n = int(input("Введите n: "))
        print_nth_symbols(string, n)
        is_continue = input("Хотите закончить (yes)? ")

    print("Спасибо за участие!")


if __name__ == '__main__':
    main()
