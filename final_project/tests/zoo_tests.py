from final_project.animal import Animal
from final_project.toucan import Toucan
from final_project.unicorn import Unicorn
from final_project.zoo import Zoo


class TestZoo:
    @classmethod
    def setup_class(cls):
        print("setup_class called once for the class")

    @classmethod
    def teardown_class(cls):
        print("teardown_class called once for the class")

    def setup_method(self):
        print("setup_method called for every method")

    def teardown_method(self):
        print("teardown_method called for every method")

    def test_init_with_file_ok(self):
        my_new_zoo = Zoo('Мой тестовый зоопарк', animals_backup_filename='animals_for_tests.pkl')
        assert my_new_zoo.title == 'Мой тестовый зоопарк'
        assert len(my_new_zoo.animals) == 2

    def test_init_without_file_ok(self):
        my_new_zoo = Zoo('Мой тестовый зоопарк', animals_backup_filename='not_exist.pkl')
        assert my_new_zoo.title == 'Мой тестовый зоопарк'
        assert my_new_zoo.animals == {}

    def test_get_title_ok(self):
        my_new_zoo = Zoo('Мой тестовый зоопарк', animals_backup_filename='animals_for_tests.pkl')
        assert my_new_zoo.get_title() == '*** Мой тестовый зоопарк ***'

    def test_get_animals_by_category_ok(self):
        my_new_zoo = Zoo('Мой тестовый зоопарк', animals_backup_filename='animals_for_tests.pkl')
        animals_by_cat = my_new_zoo.get_animals_by_category('Лошади')
        assert len(animals_by_cat) == 3

    def test_get_animals_by_category_err(self):
        my_new_zoo = Zoo('Мой тестовый зоопарк', animals_backup_filename='animals_for_tests.pkl')
        animals_by_cat = my_new_zoo.get_animals_by_category('Пчелы')
        assert animals_by_cat is None

    def test_get_animal_with_min_count(self):
        Animal.count = 0
        Toucan.count = 0
        Unicorn.count = 0

        my_zoo = Zoo(title='Самый лучший зоопарк', should_load_backup=False)

        assert my_zoo.get_animal_with_min_count() == []

        unicorn_1 = Unicorn(name='Вася', color='белый', firework_color="синего", age=5)
        unicorn_2 = Unicorn('Маша', 1, 'синий', "серебряного")
        my_zoo.add_animal('Лошади', unicorn_1)
        my_zoo.add_animal('Лошади', unicorn_2)
        toucan_1 = Toucan('Лиза', 2, 'черный', 0.75, 100)
        toucan_2 = Toucan('Петя', 6, 'черный', 1.5, 200)
        my_zoo.add_animal('Птицы', toucan_1)
        my_zoo.add_animal('Птицы', toucan_2)
        assert my_zoo.get_animal_with_min_count() == ['Unicorn', 'Toucan']

        toucan_3 = Toucan('Лена', 6, 'черный', 1.5, 200)
        my_zoo.add_animal('Птицы', toucan_3)
        assert my_zoo.get_animal_with_min_count() == ['Unicorn']

        unicorn_3 = Unicorn('Маша', 1, 'синий', "серебряного")
        my_zoo.add_animal('Лошади', unicorn_3)
        assert my_zoo.get_animal_with_min_count() == ['Unicorn', 'Toucan']

        unicorn_4 = Unicorn('Маша', 1, 'синий', "серебряного")
        my_zoo.add_animal('Лошади', unicorn_4)
        assert my_zoo.get_animal_with_min_count() == ['Toucan']
